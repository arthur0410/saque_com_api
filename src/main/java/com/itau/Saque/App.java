package com.itau.Saque;
import java.util.ArrayList;

public class App 
{
	
	private static ArrayList<String> arquivoContas;
	private static ArrayList<String> arquivoSaques;
	
	public static void main( String[] args )
	{

		arquivoContas = new ArrayList<String>();
		arquivoContas = FileManager.lerArquivoContas();
		arquivoSaques = new ArrayList<String>();
		arquivoSaques = FileManager.lerArquivoSaques();

		for (int i = 1; i < arquivoSaques.size(); i++) {
			
			String[] partesSaque = new String[5]; 
			partesSaque = arquivoSaques.get(i).split(",");
			String usernameSaque = partesSaque[0];

			for (int j = 1; j < arquivoContas.size(); j++) {
				
				Cliente cliente = new Cliente(arquivoContas.get(j));

				if(usernameSaque.equals(cliente.username)){

					double valorSaque = Double.parseDouble(partesSaque[1]);
					String moeda = partesSaque[2];
					
					if (cliente.sacarDinheiro(valorSaque, moeda)) {
						System.out.println(cliente.toString());
						arquivoContas.set(j, cliente.toString());
					} else {
						System.out.println(cliente.nome + " nao possui saldo o suficiente, valor disponivel = " + cliente.valorDisponivel);
					}
				}
			}
		}
		
		FileManager.gravarArquivoContas(arquivoContas);
	}
}
